#!/usr/bin/env python
"""
This is a placeholder generic docstring just to show you I learned about it
this is where you explain attributes, write your todo list, etc.
"""
__author__ = "Nathan Ortbals"

# I'm not sure if this notation for constants is correct
STARTING_NUM_OF_STICKS = 20
HEIGHT_OF_STICKS = 5


def print_sticks(number_of_sticks, stick_height):
    """
    Prints a certain number of sticks in the ouput using the '|' character
    :param number_of_sticks: The number of sticks to be displayed
    :param stick_height: The height of the sticks to be displayed
    :return: none
    """
    print("\n" * 100)
    for i in range(stick_height):
        for x in range(number_of_sticks):
            print("  |", end="")
            i += 1
        print("")


def get_choice(turn):
    """
    Prompts the player for input, and returns the input
    :param turn: What turn the game is on, used to determine which player needs to input
    :return: none
    """
    if turn % 2 == 0:
        return int(input("Player A, how many sticks will you remove?: "))
    else:
        return int(input("Player B, how many sticks will you remove?: "))


def main():
    """
    Loops until number of sticks has run out, while error checking user's input
    Determines winner via last turn
    :return: none
    """
    number_of_sticks = STARTING_NUM_OF_STICKS
    turn = 0
    choice = 0
    while number_of_sticks > 0:
        print_sticks(number_of_sticks, HEIGHT_OF_STICKS)
        error = 1
        while error != 0:
            choice = get_choice(turn)
            if (choice == 1 or choice == 2 or choice == 3) and choice <= number_of_sticks:
                error = 0
        number_of_sticks -= choice
        turn += 1
    if turn % 2 == 0:
        print("Player A wins!")
    else:
        print("Player B wins!")


if __name__ == "__main__":
    main()
