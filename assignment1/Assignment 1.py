#!/usr/bin/env python
"""
This is a placeholder generic docstring just to show you I learned about it
this is where you explain attributes, write your todo list, etc.
"""
__author__ = "Nathan Ortbals"

def calcpayment(principal, years, interestrate):
    """
    Formula for annual repayment of a loan
    :param principal: amount of money borrowed
    :param years: number of years rerquired to pay the loan in full
    :param interestrate: interest rate charged each year on unpaid principal
    :return: annual repayment of the loan
    """
    return (((1 + interestrate)**years) * principal * interestrate) / ((1 + interestrate)**years - 1)


def main():
    """
    Prompt user for principal of loan, number of years on loan, and interest rate
    Calculate payment and display yearlypayment, monthly payment, and total amount paid
    :return: 0 if successful
    """
    principal = int(input("Please enter the amount of money borrowed, a positive int (no commas): "))
    years = int(input("Please enter the number of years used to repay the loan, a positive int: "))
    interestrate = float(input('Please type the annual interest rate charged on the principle, a fraction'
                               '(e.g., 8.5% is typed as 0.085): '))
    yearlypayment = calcpayment(principal, years, interestrate)
    print('The annual is $%.2f' % yearlypayment,
          '\nThe monthly payment is $%.2f' % (yearlypayment/12),
          '\nThe total paid over the life of the loan is $%.2f' % (yearlypayment * years))
    return 0

if __name__ == "__main__":
    main()

